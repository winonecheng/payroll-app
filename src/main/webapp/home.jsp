<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<jsp:include page="header.jsp" />

<div class="container-fluid">
  <div class="row">
    <nav class="col-md-2 d-none d-md-block bg-light sidebar">
      <jsp:include page="menu.jsp">
        <jsp:param name="item" value="home" />
      </jsp:include>
    </nav>

    <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
      <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3">
      </div>

      <h2>Employee Salaries</h2>
      <div class="table-responsive">
        <table class="table table-sm">
          <thead>
          <tr>
            <th>#</th>
            <th>Employee Name</th>
            <th>Hourly Rate</th>
            <th>Time Cards</th>
            <th>Salary</th>
          </tr>
          </thead>
          <tbody>
          <c:forEach var="employee" items="${employees}" varStatus="loop">
            <tr>
              <td>${loop.index + 1}.</td>
              <td>${employee.name}</td>
              <td><fmt:formatNumber type="number" maxFractionDigits="0" value="${employee.hourlyRate}" /></td>
              <td>
                <table class="table table-striped table-sm">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Check-In Time</th>
                      <th>Check-Out Time</th>
                      <th>Base Hours</th>
                      <th>OT Hours</th>
                    </tr>
                  </thead>
                  <tbody>
                      <c:forEach var="timeCard" items="${employee.timeCards}" varStatus="loopTimeCard">
                        <tr>
                          <td>${loopTimeCard.index + 1}.</td>
                          <td><fmt:formatDate value="${timeCard.checkinDate}" pattern="yyyy-MM-dd HH:mm:ss" /></td>
                          <td><fmt:formatDate value="${timeCard.checkoutDate}" pattern="yyyy-MM-dd HH:mm:ss" /></td>
                          <td>${timeCard.baseHours}</td>
                          <td>${timeCard.overTimeHours}</td>
                        </tr>
                      </c:forEach>
                  </tbody>
                </table>
              </td>
              <td>${employee.calculatePay()}</td>
            </tr>
          </c:forEach>
          </tbody>
        </table>
      </div>
    </main>
  </div>
</div>

<jsp:include page="footer.jsp" />

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
<script src="../../assets/js/vendor/popper.min.js"></script>
<script src="../../dist/js/bootstrap.min.js"></script>

<!-- Icons -->
<script src="https://unpkg.com/feather-icons/dist/feather.min.js"></script>
<script>
  feather.replace()
</script>
</body>
</html>
